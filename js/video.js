$(document).ready(function() {

// Gets the video src from the data-src on each button

var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "src" );
});
console.log($videoSrc);

  
  
// when the modal is opened autoplay it  
$('#myModal').on('shown.bs.modal', function (e) {
    
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
})
  


// stop playing the youtube video when I close the modal
$('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src',$videoSrc); 
});


$(document).on('click','#block-id-02.in',function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide');
    }
});
$(".expanded").click(function(){
    $("#mycollapse").collapse('hide');

});

$("#readless-01").hide();
$("#readless-02").hide();
$("#readless-03").hide();
$("#readless-1").hide();
$("#readless-2").hide();
$("#readless-3").hide();
$("#readless-4").hide();


// 1
$("#readmore-01").click(function(){  
    $("#readless-01").show("slow");
    $("#block-id-01").show("slow");
    $("#readmore-01").hide();
});

$("#readless-01").click(function(){  
    $("#readmore-01").show();
    $("#block-id-01").hide();
    $("#readless-01").hide();
});
// 2
$("#readmore-02").click(function(){  
    $("#readless-02").show("slow");
    $("#block-id-02").show("slow");
    $("#readmore-02").hide();
});

$("#readless-02").click(function(){  
    $("#readmore-02").show();
    $("#block-id-02").hide();
    $("#readless-02").hide();
});

// 3
$("#readmore-03").click(function(){  
    $("#readless-03").show("slow");
    $("#block-id-03").show("slow");
    $("#readmore-03").hide();
});

$("#readless-03").click(function(){  
    $("#readmore-03").show();
    $("#block-id-03").hide();
    $("#readless-03").hide();
});
/* ============================*/
$("#readmore-1").click(function(){  
    $("#readless-1").show("slow");
    $("#block-id-1").show("slow");
    $("#readmore-1").hide();
});

$("#readless-1").click(function(){  
    $("#readmore-1").show();
    $("#block-id-1").hide();
    $("#readless-1").hide();
});
// 2
$("#readmore-2").click(function(){  
    $("#readless-2").show("slow");
    $("#block-id-2").show("slow");
    $("#readmore-2").hide();
});

$("#readless-2").click(function(){  
    $("#readmore-2").show();
    $("#block-id-2").hide();
    $("#readless-2").hide();
});

// 3
$("#readmore-3").click(function(){  
    $("#readless-3").show("slow");
    $("#block-id-3").show("slow");
    $("#readmore-3").hide();
});

$("#readless-3").click(function(){  
    $("#readmore-3").show();
    $("#block-id-3").hide();
    $("#readless-3").hide();
});

// 3
$("#readmore-4").click(function(){  
    $("#readless-4").show("slow");
    $("#block-id-4").show("slow");
    $("#readmore-4").hide();
});

$("#readless-4").click(function(){  
    $("#readmore-4").show();
    $("#block-id-4").hide();
    $("#readless-4").hide();
});
// document ready  
});